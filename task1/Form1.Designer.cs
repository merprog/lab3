﻿
namespace task1
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonStart = new System.Windows.Forms.Button();
            this.labelMemory = new System.Windows.Forms.Label();
            this.labelComputerName = new System.Windows.Forms.Label();
            this.labelUserName = new System.Windows.Forms.Label();
            this.systemDirectoryLabel = new System.Windows.Forms.Label();
            this.tempDirectoryLabel = new System.Windows.Forms.Label();
            this.currentDirectoryLabel = new System.Windows.Forms.Label();
            this.textBoxLog = new System.Windows.Forms.TextBox();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.SuspendLayout();
            // 
            // buttonStart
            // 
            this.buttonStart.Location = new System.Drawing.Point(339, 217);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(121, 43);
            this.buttonStart.TabIndex = 0;
            this.buttonStart.Text = "Start";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // labelMemory
            // 
            this.labelMemory.AutoSize = true;
            this.labelMemory.Location = new System.Drawing.Point(12, 289);
            this.labelMemory.Name = "labelMemory";
            this.labelMemory.Size = new System.Drawing.Size(38, 15);
            this.labelMemory.TabIndex = 1;
            this.labelMemory.Text = "label1";
            // 
            // labelComputerName
            // 
            this.labelComputerName.AutoSize = true;
            this.labelComputerName.Location = new System.Drawing.Point(12, 316);
            this.labelComputerName.Name = "labelComputerName";
            this.labelComputerName.Size = new System.Drawing.Size(38, 15);
            this.labelComputerName.TabIndex = 2;
            this.labelComputerName.Text = "label1";
            // 
            // labelUserName
            // 
            this.labelUserName.AutoSize = true;
            this.labelUserName.Location = new System.Drawing.Point(12, 342);
            this.labelUserName.Name = "labelUserName";
            this.labelUserName.Size = new System.Drawing.Size(38, 15);
            this.labelUserName.TabIndex = 3;
            this.labelUserName.Text = "label1";
            // 
            // systemDirectoryLabel
            // 
            this.systemDirectoryLabel.AutoSize = true;
            this.systemDirectoryLabel.Location = new System.Drawing.Point(12, 367);
            this.systemDirectoryLabel.Name = "systemDirectoryLabel";
            this.systemDirectoryLabel.Size = new System.Drawing.Size(38, 15);
            this.systemDirectoryLabel.TabIndex = 4;
            this.systemDirectoryLabel.Text = "label1";
            // 
            // tempDirectoryLabel
            // 
            this.tempDirectoryLabel.AutoSize = true;
            this.tempDirectoryLabel.Location = new System.Drawing.Point(12, 392);
            this.tempDirectoryLabel.Name = "tempDirectoryLabel";
            this.tempDirectoryLabel.Size = new System.Drawing.Size(38, 15);
            this.tempDirectoryLabel.TabIndex = 5;
            this.tempDirectoryLabel.Text = "label1";
            // 
            // currentDirectoryLabel
            // 
            this.currentDirectoryLabel.AutoSize = true;
            this.currentDirectoryLabel.Location = new System.Drawing.Point(12, 417);
            this.currentDirectoryLabel.Name = "currentDirectoryLabel";
            this.currentDirectoryLabel.Size = new System.Drawing.Size(38, 15);
            this.currentDirectoryLabel.TabIndex = 6;
            this.currentDirectoryLabel.Text = "label1";
            // 
            // textBoxLog
            // 
            this.textBoxLog.Location = new System.Drawing.Point(12, 121);
            this.textBoxLog.Multiline = true;
            this.textBoxLog.Name = "textBoxLog";
            this.textBoxLog.Size = new System.Drawing.Size(776, 90);
            this.textBoxLog.TabIndex = 7;
            // 
            // folderBrowserDialog1
            // 
            this.folderBrowserDialog1.UseDescriptionForTitle = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.textBoxLog);
            this.Controls.Add(this.currentDirectoryLabel);
            this.Controls.Add(this.tempDirectoryLabel);
            this.Controls.Add(this.systemDirectoryLabel);
            this.Controls.Add(this.labelUserName);
            this.Controls.Add(this.labelComputerName);
            this.Controls.Add(this.labelMemory);
            this.Controls.Add(this.buttonStart);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.Label labelMemory;
        private System.Windows.Forms.Label labelComputerName;
        private System.Windows.Forms.Label labelUserName;
        private System.Windows.Forms.Label systemDirectoryLabel;
        private System.Windows.Forms.Label tempDirectoryLabel;
        private System.Windows.Forms.Label currentDirectoryLabel;
        private System.Windows.Forms.TextBox textBoxLog;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
    }
}


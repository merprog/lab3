﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Microsoft.VisualBasic.Devices;

namespace task1
{
    public partial class Form1 : Form
    {
        private ListView listViewDrives;
        private FileSystemWatcher fileSystemWatcher;
        private string selectedDirectory;

        public Form1()
        {
            InitializeComponent();

            listViewDrives = new ListView();
            listViewDrives.Dock = DockStyle.Fill;
            listViewDrives.View = View.Details;
            listViewDrives.FullRowSelect = true;

            listViewDrives.Columns.Add("Диск", 150);
            listViewDrives.Columns.Add("Тип", 100);
            listViewDrives.Columns.Add("Файлова система", 150);
            listViewDrives.Columns.Add("Зайнято (байт)", 120);
            listViewDrives.Columns.Add("Вільно (байт)", 120);

            Controls.Add(listViewDrives);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // Отримуємо усі диски
            DriveInfo[] drives = DriveInfo.GetDrives();
            foreach (DriveInfo drive in drives)
            {
                // Для кожного диску створюємо новий рядок в listview
                ListViewItem item = new ListViewItem(drive.Name);
                item.SubItems.Add(drive.DriveType.ToString());
                item.SubItems.Add(drive.DriveFormat);
                item.SubItems.Add((drive.TotalSize - drive.TotalFreeSpace).ToString());
                item.SubItems.Add(drive.TotalFreeSpace.ToString());
                listViewDrives.Items.Add(item);
            }

            // Отримуємо інформацію про загальну пам'ять
            ComputerInfo computerInfo = new ComputerInfo();
            labelMemory.Text = $"Загальний обсяг пам'яті: {computerInfo.TotalPhysicalMemory} байт";

            // Отримуємо інформацію про ім'я комп'ютера
            string computerName = Environment.MachineName;
            labelComputerName.Text = $"Назва комп'ютера: {computerName}";

            // Отримуємо інформацію про ім'я користувача
            string userName = Environment.UserName;
            labelUserName.Text = $"Поточний користувач: {userName}";

            // Отримуємо системну папку
            string systemDirectory = Environment.SystemDirectory;
            systemDirectoryLabel.Text = $"Системний каталог: {systemDirectory}";

            // Отримуємо тимчасову папку
            string tempDirectory = Path.GetTempPath();
            tempDirectoryLabel.Text = $"Тимчасовий каталог: {tempDirectory}";

            // Отримуємо поточну папку
            string currentDirectory = Environment.CurrentDirectory;
            currentDirectoryLabel.Text = $"Поточний робочий каталог: {currentDirectory}";
        }

        private void LogEvent(string logMessage)
        {
            if (textBoxLog.InvokeRequired)
            {
                textBoxLog.Invoke(new Action<string>(LogEvent), logMessage);
            }
            else
            {
                textBoxLog.AppendText($"{DateTime.Now:G}: {logMessage}\r\n");
                File.AppendAllText("log.txt", $"{DateTime.Now:G}: {logMessage}\r\n");
            }
        }

        private void OnChanged(object source, FileSystemEventArgs e)
        {
            LogEvent($"Змінено: {e.FullPath}");
        }

        private void OnRenamed(object source, RenamedEventArgs e)
        {
            LogEvent($"Перейменовано: {e.OldFullPath} на {e.FullPath}");
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                // за допомогою filewatcher слідкуємо за зміною стану папок
                selectedDirectory = folderBrowserDialog1.SelectedPath;
                fileSystemWatcher = new FileSystemWatcher(selectedDirectory);
                fileSystemWatcher.EnableRaisingEvents = true;
                fileSystemWatcher.IncludeSubdirectories = true;
                fileSystemWatcher.NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.FileName | NotifyFilters.DirectoryName;
                fileSystemWatcher.Changed += new FileSystemEventHandler(OnChanged);
                fileSystemWatcher.Created += new FileSystemEventHandler(OnChanged);
                fileSystemWatcher.Deleted += new FileSystemEventHandler(OnChanged);
                fileSystemWatcher.Renamed += new RenamedEventHandler(OnRenamed);

                LogEvent($"Початок спостереження за каталогом {selectedDirectory}");
            }
        }
    }
}
